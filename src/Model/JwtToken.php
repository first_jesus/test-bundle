<?php

namespace Test\Bundle\SecurityBundle\Model;

class JwtToken
{

    public function __construct(
        private readonly array  $header,
        private readonly array  $payload,
        private readonly string $verifySignature
    )
    {
    }

    public function getHeader(): array
    {
        return $this->header;
    }

    public function getPayload(): array
    {
        return $this->payload;
    }

    public function getVerifySignature(): string
    {
        return $this->verifySignature;
    }

}