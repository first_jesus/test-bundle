<?php

namespace Test\Bundle\SecurityBundle\Exception;

use App\Common\Domain\Exception\NotFoundException;

class ProfileNotFoundException extends NotFoundException
{

}