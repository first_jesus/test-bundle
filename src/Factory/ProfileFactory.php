<?php

namespace Test\Bundle\SecurityBundle\Factory;

use Test\Bundle\SecurityBundle\Dto\ISignUp;
use Test\Bundle\SecurityBundle\Dto\SignUpDto;
use Test\Bundle\SecurityBundle\Dto\SignUpEmployeeDto;
use Test\Bundle\SecurityBundle\Dto\UpdateProfileDto;
use Test\Bundle\SecurityBundle\Entity\Profile;
use Symfony\Component\Security\Core\User\UserInterface;

class ProfileFactory
{

    public function create(ISignUp $dto, int $userId): Profile
    {
        return match (get_class($dto)) {
            SignUpDto::class => $this->createUser($dto, $userId),
            SignUpEmployeeDto::class => $this->createEmployee($dto, $userId)
        };
    }

    /**
     * @param SignUpDto $dto
     * @param int $userId
     * @return Profile
     */
    private function createUser(ISignUp $dto, int $userId): Profile
    {
        return new Profile(
            phone: $dto->getPhone(),
            userId: $userId,
            isEmployee: false,
            firstName: $dto->getFirstName()
        );
    }

    /**
     * @param SignUpEmployeeDto $dto
     * @param int $userId
     * @return Profile
     */
    private function createEmployee(ISignUp $dto, int $userId): Profile
    {
        return new Profile(
            phone: $dto->getPhone(),
            userId: $userId,
            isEmployee: true,
            firstName: $dto->getFirstName(),
            middleName: $dto->getMiddleName(),
            lastName: $dto->getLastName(),
            filialIds: $dto->getFilialIds(),
            email: $dto->getEmail()
        );
    }


    /**
     * @param Profile $profile
     * @param UpdateProfileDto $dto
     * @return Profile
     */
    public function update(UserInterface $profile, UpdateProfileDto $dto): Profile
    {
        if ($dto->getEmail()) {
            $profile->setEmail($dto->getEmail());
        }

        if ($dto->getFirstName()) {
            $profile->setFirstName($dto->getFirstName());
        }

        if ($dto->getMiddleName()) {
            $profile->setMiddleName($dto->getMiddleName());
        }

        if ($dto->getLastName()) {
            $profile->setLastName($dto->getLastName());
        }

        if ($dto->getFilialIds() && $profile->isEmployee()) {
            $profile->setFilialIds($dto->getFilialIds());
        }

        return $profile;
    }

}