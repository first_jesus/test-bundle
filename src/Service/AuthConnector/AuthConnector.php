<?php

namespace Test\Bundle\SecurityBundle\Service\AuthConnector;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

class AuthConnector
{

    public function __construct(
        private readonly Client $client
    )
    {
    }

    /**
     * @throws GuzzleException
     */
    public function signUp(string $userName, string $password, ?string $firstName = null): array
    {
        return $this->call(
            uri: '/signUp',
            data: [
                'username' => $userName,
                'password' => $password,
                'name' => $firstName
            ]
        );
    }

    /**
     * @throws GuzzleException
     */
    private function call(string $uri, array $data): array
    {
        $response = $this->client->post(
            uri: $_ENV['AUTH_URL'] . $uri,
            options: [
                RequestOptions::TIMEOUT => 60,
                RequestOptions::CONNECT_TIMEOUT => 30,
                RequestOptions::HEADERS => [
                    'Content-Type' => 'application/json',
                ],
                RequestOptions::BODY => json_encode($data)
            ]
        );

        return json_decode($response->getBody()->getContents(), true);
    }

}