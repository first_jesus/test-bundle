<?php

namespace Test\Bundle\SecurityBundle\Service\AuthService;

use App\Common\Infrastructure\Service\Validator\Exception\ValidateException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use Test\Bundle\SecurityBundle\Dto\ISignUp;
use Test\Bundle\SecurityBundle\Entity\IProfileRepository;
use Test\Bundle\SecurityBundle\Exception\AuthConnectorException;
use Test\Bundle\SecurityBundle\Exception\ProfileSaveException;
use Test\Bundle\SecurityBundle\Factory\ProfileFactory;
use Test\Bundle\SecurityBundle\Service\AuthConnector\AuthConnector;
use Psr\Log\LoggerInterface;

class UserAuthService
{

    public function __construct(
        private readonly AuthConnector      $connector,
        private readonly ProfileFactory     $factory,
        private readonly IProfileRepository $repository,
        private readonly LoggerInterface    $logger
    )
    {
    }

    /**
     * @throws ValidateException
     * @throws AuthConnectorException
     */
    public function signUp(ISignUp $dto): ?int
    {
        //получаем ответ от сервиса авторизации
        try {
            $response = $this->connector->signUp(
                userName: $dto->getPhone(),
                password: $dto->getPassword(),
                firstName: $dto->getFirstName()
            );
        } catch (ConnectException $e) {
            $this->logger->error("Не удалось зарегистрировать пользователя {$dto->getPhone()} - auth не отвечает. {$e->getMessage()}");
            throw new AuthConnectorException("Сервис авторизации не отвечает");
        } catch (ClientException $e) {
            $error = json_decode($e->getResponse()->getBody()->getContents(), true);
            $this->logger->error("Не удалось зарегистрировать пользователя {$dto->getPhone()} - {$error['message']}");
            throw new AuthConnectorException($error['message']);
        } catch (GuzzleException $e) {
            $error = json_decode($e->getResponse()->getBody()->getContents(), true);
            $this->logger->error("Не удалось зарегистрировать пользователя {$dto->getPhone()} - {$error['message']}");
            throw new ValidateException($error['message']);
        }

        if (!array_key_exists('userId', $response['data'])) {
            $this->logger->error("Отсутствует userId после регистрации пользователем {$dto->getPhone()}");
            throw new AuthConnectorException("Не удалось зарегистрироваться");
        }

        try {
            //создаем и сохраняем пользователя с минимальными данными, которые у нас есть
            $profile = $this->factory->create(
                dto: $dto,
                userId: $response['data']['userId']
            );
            $this->repository->save($profile);

            if ($profile->isEmployee()) {
                return $profile->getUserId();
            }
        } catch (ProfileSaveException $e) {
            //ничего не делаем, если пользователь решил зарегаться, а до этого положил болт (запись уже есть)
            $this->logger->error("Не удалось создать профиль для {$dto->getPhone()} - {$e->getMessage()}");
        }

        return null;
    }

}