<?php

namespace Test\Bundle\SecurityBundle\Service\JWT;

use Test\Bundle\SecurityBundle\Model\JwtToken;

interface IJWTManager
{

    public function encode(array $data): string;

    public function decode(string $token): JwtToken;

}