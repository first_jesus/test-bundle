<?php

namespace Test\Bundle\SecurityBundle\Service\JWT;

use Test\Bundle\SecurityBundle\Exception\JWTFailureException;
use Test\Bundle\SecurityBundle\Model\JwtToken;

class JwtService implements IJWTManager
{

    /**
     * @throws JWTFailureException
     */
    public function encode(array $data): string
    {
        throw new JWTFailureException('Not support');
    }

    public function decode(string $token): JwtToken
    {
        list($header, $payload, $verifySignature) = explode('.', $token);
        return new JwtToken(
            json_decode(base64_decode($header), true),
            json_decode(base64_decode($payload), true),
            $verifySignature
        );
    }
}