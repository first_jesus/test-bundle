<?php

namespace App\Common\Infrastructure;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class JsonResponseFormatter {

    public static function ok(mixed $data = [], string $message = 'OK'): JsonResponse {
        return new JsonResponse([
            'data'      => $data,
            'message'   => $message,
            'code'      => Response::HTTP_OK
        ]);
    }

    public static function created(mixed $data = []): JsonResponse {
        return new JsonResponse([
            'data'      => $data,
            'message'   => 'OK',
            'code'      => Response::HTTP_CREATED
        ], Response::HTTP_CREATED);
    }

    public static function internalServerError(string $errorMessage = 'Internal error'): JsonResponse {
        return new JsonResponse([
            'data'      => [],
            'message'   => $errorMessage,
            'code'      => Response::HTTP_INTERNAL_SERVER_ERROR
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public static function badRequest(string $errorMessage): JsonResponse {
        return new JsonResponse([
            'data'      => [],
            'message'   => $errorMessage,
            'code'      => Response::HTTP_BAD_REQUEST
        ], Response::HTTP_BAD_REQUEST);
    }

    public static function unauthorized(string $errorMessage): JsonResponse {
        return new JsonResponse([
            'data'      => [],
            'message'   => $errorMessage,
            'code'      => Response::HTTP_UNAUTHORIZED
        ], Response::HTTP_UNAUTHORIZED);
    }

    public static function notFound(string $errorMessage = 'Not Found'): JsonResponse {
        return new JsonResponse([
            'data'      => [],
            'message'   => $errorMessage,
            'code'      => Response::HTTP_NOT_FOUND
        ], Response::HTTP_NOT_FOUND);
    }

    public static function forbidden(string $errorMessage = 'Forbidden'): JsonResponse {
        return new JsonResponse([
            'data'      => [],
            'message'   => $errorMessage,
            'code'      => Response::HTTP_FORBIDDEN
        ], Response::HTTP_FORBIDDEN);
    }

    /**
     * @param array $data
     * @param int   $total - кол-во записей всего
     * @param int   $limit - кол-во записей на страницу
     * @param int   $perPage - кол-во записей на текущей странице
     * @param int   $currentPage - текущая страницы
     * @param int   $totalPage - кол-во страниц
     *
     * @return JsonResponse
     */
    public static function pagination(
        array   $data,
        int     $total,
        int     $limit,
        int     $perPage,
        int     $currentPage,
        int     $totalPage
    ): JsonResponse {
        return new JsonResponse([
            'data'          => $data,
            'message'       => 'OK',
            'total'         => $total,
            'limit'         => $limit,
            'perPage'       => $perPage,
            'currentPage'   => $currentPage,
            'totalPage'     => $totalPage
        ], Response::HTTP_OK);
    }

}