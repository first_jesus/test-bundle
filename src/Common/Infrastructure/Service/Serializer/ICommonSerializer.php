<?php

namespace App\Common\Infrastructure\Service\Serializer;

interface ICommonSerializer {

    public function arrayToXml(array $arr): string;

    public function xmlToArray(string $xml): array;

    public function xmlToArrayObject(string $xml, string $arrayClassname): array;

    public function jsonToObject(string $json, string $classname, array $params = []): object;

    public function objectToJson(object $object, array $params = []): string;

    public function jsonToArrayObject(string $json, string $arrayClassname, array $params = []): array;

    public function arrayObjectToJson(array $arrayObject, array $params = []): string;

    public function arrayToObject(array $arrayData, string $classname): object;

}