<?php

namespace App\Common\Infrastructure\Service\Serializer;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;

class CommonSerializer implements ICommonSerializer {

    const FORMAT_XML = 'xml';
    const FORMAT_JSON = 'json';

    private XmlEncoder $encoder;
    private Serializer $serializer;

    public function __construct(string $rootNodeName = 'data', bool $isDeclareMainXmlNode = true)
    {
        $this->encoder = new XmlEncoder([
            "xml_root_node_name" => $rootNodeName,
            "xml_encoding" => 'utf-8',
            'load_options' => \LIBXML_PARSEHUGE | \LIBXML_NONET | \LIBXML_NOBLANKS | \LIBXML_NOXMLDECL | \LIBXML_COMPACT | \LIBXML_NSCLEAN,
            'encoder_ignored_node_types' => [
                $isDeclareMainXmlNode === false ? \XML_PI_NODE : null,
            ],
        ]);

        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));

        $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);

        $this->serializer = new Serializer(
            [new ObjectNormalizer($classMetadataFactory, $metadataAwareNameConverter)],
            ['json' => new JsonEncoder()]
        );
    }

    public function arrayToXml(mixed $arr): string
    {
        return $this->utf8_for_xml($this->encoder->encode($arr, self::FORMAT_XML));
    }

    public function xmlToArray(string $xml): array
    {
        return $this->encoder->decode($this->utf8_for_xml($xml), self::FORMAT_XML);
    }

    public function arrayObjectToJson(array $arrayObject, array $params = []): string
    {
        return $this->serializer->serialize($arrayObject, self::FORMAT_JSON, $this->prepareSystemFields($params));
    }

    public function jsonToArrayObject(string $json, string $arrayClassname, array $params = []): array
    {
        return $this->serializer->deserialize($json, $arrayClassname, null, $this->prepareSystemFields($params));
    }

    public function jsonToObject(string $json, string $classname, array $params = []): object
    {
        return $this->serializer->deserialize($json, $classname, self::FORMAT_JSON, $this->prepareSystemFields($params));
    }

    public function objectToJson(mixed $object, array $params = []): string
    {
        return $this->serializer->serialize($object, self::FORMAT_JSON, $this->prepareSystemFields($params));
    }

    public function xmlToArrayObject(string $xml, string $arrayClassname): array
    {
        return $this->jsonToArrayObject(
            json_encode($this->xmlToArray($xml)),
            $arrayClassname
        );
    }

    private function prepareSystemFields(array $params = []): array {
        if (array_key_exists('groups', $params)) {
            match (gettype($params['groups'])) {
                'array' => $params['groups'][] = 'system',
                'string' => $params['groups'] = [$params['groups'], 'system'],
                default => $params
            };
        }
        return $params;
    }

    /**
     * @throws ExceptionInterface
     */
    public function arrayToObject(array $arrayData, string $classname): object
    {
        return $this->serializer->denormalize($arrayData, $classname);
    }

    private function utf8_for_xml($string): array|string|null
    {
        return preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', '', $string);
    }

}