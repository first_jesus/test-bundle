<?php

namespace App\Common\Infrastructure\Service\Validator;

use App\Common\Infrastructure\Service\Validator\Exception\ValidateException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Exception;

class Validator implements IValidator {

    public function __construct(
        private ValidatorInterface $validator
    )
    {
    }

    /**
     * @throws Exception | ValidateException
     */
    public function validate(object $object): ?ValidateException
    {
        $errors = $this->validator->validate($object);
        if ($errors->count()) {
            $messages = array_map(function (ConstraintViolation $item) {
                return $item->getMessage();
            }, iterator_to_array($errors->getIterator()));
            $errorMessage = implode('; ', $messages);
            throw new ValidateException($errorMessage);
        }

        return null;
    }
}