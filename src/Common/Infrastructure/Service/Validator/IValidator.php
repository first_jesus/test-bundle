<?php

namespace App\Common\Infrastructure\Service\Validator;

use App\Common\Infrastructure\Service\Validator\Exception\ValidateException;

interface IValidator {

    /**
     * @throws  ValidateException
     */
    public function validate(object $object): ?ValidateException;

}