<?php

namespace App\Common\Infrastructure\Persistence;

use Doctrine\Common\EventSubscriber;
use Doctrine\DBAL\Event\SchemaIndexDefinitionEventArgs;
use Doctrine\DBAL\Events;

class DoctrineSchemaListener implements EventSubscriber
{

    /**
     * @Description - если имя инлекса содержит index_gin то diff игнорит его
     * @param SchemaIndexDefinitionEventArgs $eventArgs
     * @return void
     */
    public function onSchemaIndexDefinition(SchemaIndexDefinitionEventArgs $eventArgs): void
    {
        if (str_contains($eventArgs->getTableIndex()['name'], 'index_gin')) {
            $eventArgs->preventDefault();
        }
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return string[]
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::onSchemaIndexDefinition,
        ];
    }
}