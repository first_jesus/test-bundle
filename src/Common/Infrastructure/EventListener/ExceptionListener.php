<?php

namespace App\Common\Infrastructure\EventListener;

use App\Common\Domain\Exception\NotFoundException;
use App\Common\Infrastructure\JsonResponseFormatter;
use App\Common\Infrastructure\Service\Validator\Exception\ValidateException;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\Messenger\Exception\HandlerFailedException;

class ExceptionListener
{

    public function __invoke(ExceptionEvent $event): void
    {
        $throwable = $event->getThrowable();
        if ($throwable instanceof ValidateException) {
            $event->setResponse(JsonResponseFormatter::badRequest($throwable->getMessage()));
            return;
        }

        if ($throwable instanceof HandlerFailedException) {
            if ($throwable->getPrevious() instanceof NotFoundException) {
                $event->setResponse(JsonResponseFormatter::notFound($throwable->getPrevious()->getMessage()));
                return;
            }

            $event->setResponse(JsonResponseFormatter::badRequest($throwable->getPrevious()->getMessage()));
            return;
        }

        $event->setResponse(JsonResponseFormatter::internalServerError($throwable->getMessage()));
    }

}