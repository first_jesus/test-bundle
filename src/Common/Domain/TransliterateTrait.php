<?php

namespace App\Common\Domain;

trait TransliterateTrait {
    private static array $alphabet = [
        'а' => 'a',     'б' => 'b',	    'в' => 'v',     'г' => 'g',	    'д' => 'd',
        'е' => 'e',	    'ё' => 'e',	    'ж' => 'j',	    'з' => 'z',	    'и' => 'i',
        'й' => 'i',	    'к' => 'k',	    'л' => 'l',	    'м' => 'm',	    'н' => 'n',
        'о' => 'o',	    'п' => 'p',	    'р' => 'r',	    'с' => 's',	    'т' => 't',
        'у' => 'u',	    'ф' => 'f',     'х' => 'h',	    'ц' => 'c',	    'ч' => 'ch',
        'ш' => 'sh',    'щ' => 'csh',	'ь' => '',	    'ы' => 'y',	    'ъ' => '',
        'э' => 'e',	    'ю' => 'yu',	'я' => 'ya',	' ' => '-',     '/' => '_',
        '"' => '',	    "'" => '',      '.' => '.',     ',' => ','
    ];
    
    public static function transliterate(string $title): string {
        $title = mb_strtolower($title);
        $title = mb_ereg_replace('/[^ a-zа-яё\d]/ui', '', $title);
        $title = strtr($title, self::$alphabet);
        return trim($title, '-');
    }
}