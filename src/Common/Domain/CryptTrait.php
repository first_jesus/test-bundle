<?php

namespace App\Common\Domain;

use RuntimeException;
use Exception;

trait CryptTrait
{
    private string $key = 'Cfyzltkftn[eqy.'; //todo: вынести
    private string $method = 'AES-256-CBC';

    /**
     * @throws Exception
     */
    public function encrypt(string $value): string
    {
        $iv = random_bytes(16);
        $value = openssl_encrypt($value, $this->method, $this->key, 0, $iv);

        $json = json_encode([
            'iv' => base64_encode($iv),
            'value' => $value
        ]);

        return base64_encode($json);
    }

    public function decrypt(string $value): string
    {
        $data = json_decode(base64_decode($value), true);

        if (!$this->validOptions($data)) {
            throw new RuntimeException('Передана некорректная строка для расшифровки');
        }

        $iv = base64_decode($data['iv']);
        return openssl_decrypt($data['value'], $this->method, $this->key, 0, $iv);
    }

    private function validOptions($options): bool
    {
        if (!is_array($options)) {
            return false;
        }

        return isset($options['iv'], $options['value']);
    }

}