<?php

namespace App\Common\Domain;

use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Symfony\Component\Serializer\Annotation\Groups;

trait TimestampsTrait
{

    #[ORM\Column(name: "created_at", type: "datetime")]
    #[Groups(['system'])]
    private DateTime $createdAt;

    #[ORM\Column(name: "updated_at", type: "datetime")]
    #[Groups(['system'])]
    private DateTime $updatedAt;

    #[ORM\PrePersist]
    public function prePersist(): void
    {
        $this->createdAt = new DateTime();
        $this->updatedAt = new DateTime();
    }

    #[ORM\PreUpdate]
    public function preUpdate(): void
    {
        $this->updatedAt = new DateTime();
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt->format('Y-m-d H:i:s');
    }

    public function getUpdatedAt(): string
    {
        return $this->updatedAt->format('Y-m-d H:i:s');
    }
}
