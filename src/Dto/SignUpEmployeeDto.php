<?php

namespace Test\Bundle\SecurityBundle\Dto;

use App\Common\App\Dto\IRequestDto;
use Symfony\Component\Validator\Constraints as Assert;

class SignUpEmployeeDto implements IRequestDto, ISignUp
{

    #[Assert\NotBlank(message: "Поле phone обязательно для заполнения")]
    #[Assert\Regex(pattern: '/^7\d{10}$/', message: 'Поле phone не является номером телефона')]
    #[Assert\Length(
        min: 11,
        max: 11,
        minMessage: "Поле userName должно быть не короче {{ limit }} символов",
        maxMessage: "Поле userName должно быть не более {{ limit }} символов"
    )]
    private string $phone;

    #[Assert\NotBlank(message: "Поле password обязательно для заполнения")]
    #[Assert\Length(
        min: 1,
        max: 255,
        minMessage: "Поле password должно быть не короче {{ limit }} символов",
        maxMessage: "Поле password должно быть не более {{ limit }} символов"
    )]
    private string $password;

    #[Assert\NotBlank(message: "Поле firstName обязательно для заполнения")]
    #[Assert\Length(
        min: 1,
        max: 255,
        minMessage: "Поле firstName должно быть не короче {{ limit }} символов",
        maxMessage: "Поле firstName должно быть не более {{ limit }} символов"
    )]
    private string $firstName;

    #[Assert\NotBlank(message: "Поле middleName обязательно для заполнения")]
    #[Assert\Length(
        min: 1,
        max: 255,
        minMessage: "Поле middleName должно быть не короче {{ limit }} символов",
        maxMessage: "Поле middleName должно быть не более {{ limit }} символов"
    )]
    private string $middleName;

    #[Assert\NotBlank(message: "Поле lastName обязательно для заполнения")]
    #[Assert\Length(
        min: 1,
        max: 255,
        minMessage: "Поле lastName должно быть не короче {{ limit }} символов",
        maxMessage: "Поле lastName должно быть не более {{ limit }} символов"
    )]
    private string $lastName;

    #[Assert\Email(message: "Поле email не является почтой")]
    private ?string $email = null;

    #[Assert\All([
        new Assert\Positive(message: "Значения в поле filialIds должны быть положительными")
    ])]
    private ?array $filialIds = null;

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getMiddleName(): string
    {
        return $this->middleName;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getFilialIds(): ?array
    {
        return $this->filialIds;
    }

    public function setEmail(?string $email): SignUpEmployeeDto
    {
        $this->email = $email;
        return $this;
    }

    public function setFirstName(string $firstName): SignUpEmployeeDto
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function setLastName(string $lastName): SignUpEmployeeDto
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function setMiddleName(string $middleName): SignUpEmployeeDto
    {
        $this->middleName = $middleName;
        return $this;
    }

    public function setPassword(string $password): SignUpEmployeeDto
    {
        $this->password = $password;
        return $this;
    }

    public function setPhone(string $phone): SignUpEmployeeDto
    {
        $this->phone = $phone;
        return $this;
    }

    public function setFilialIds(?array $filialIds): SignUpEmployeeDto
    {
        $this->filialIds = $filialIds;
        return $this;
    }
}