<?php

namespace Test\Bundle\SecurityBundle\Dto;

use App\Common\App\Dto\IRequestDto;
use Symfony\Component\Validator\Constraints as Assert;

class SignUpDto implements IRequestDto, ISignUp
{

    #[Assert\NotBlank(message: "Поле phone обязательно для заполнения")]
    #[Assert\Regex(pattern: '/^7\d{10}$/', message: 'Поле phone не является номером телефона')]
    #[Assert\Length(
        min: 11,
        max: 11,
        minMessage: "Поле userName должно быть не короче {{ limit }} символов",
        maxMessage: "Поле userName должно быть не более {{ limit }} символов"
    )]
    private string $phone;

    #[Assert\NotBlank(message: "Поле password обязательно для заполнения")]
    #[Assert\Length(
        min: 1,
        max: 255,
        minMessage: "Поле password должно быть не короче {{ limit }} символов",
        maxMessage: "Поле password должно быть не более {{ limit }} символов"
    )]
    private string $password;

    #[Assert\Length(
        min: 1,
        max: 255,
        minMessage: "Поле firstName должно быть не короче {{ limit }} символов",
        maxMessage: "Поле firstName должно быть не более {{ limit }} символов"
    )]
    private ?string $firstName = null;
    
    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setPhone(string $phone): SignUpDto
    {
        $this->phone = $phone;
        return $this;
    }

    public function setPassword(string $password): SignUpDto
    {
        $this->password = $password;
        return $this;
    }
    
    public function setFirstName(?string $firstName): SignUpDto
    {
        $this->firstName = $firstName;
        return $this;
    }
    
}