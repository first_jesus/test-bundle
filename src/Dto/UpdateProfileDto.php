<?php

namespace Test\Bundle\SecurityBundle\Dto;

use App\Common\App\Dto\IRequestDto;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateProfileDto implements IRequestDto
{

    #[Assert\Length(
        min: 1,
        max: 255,
        minMessage: "Поле firstName должно быть не короче {{ limit }} символов",
        maxMessage: "Поле firstName должно быть не более {{ limit }} символов"
    )]
    private ?string $firstName = null;

    #[Assert\Length(
        min: 1,
        max: 255,
        minMessage: "Поле middleName должно быть не короче {{ limit }} символов",
        maxMessage: "Поле middleName должно быть не более {{ limit }} символов"
    )]
    private ?string $middleName = null;

    #[Assert\Length(
        min: 1,
        max: 255,
        minMessage: "Поле lastName должно быть не короче {{ limit }} символов",
        maxMessage: "Поле lastName должно быть не более {{ limit }} символов"
    )]
    private ?string $lastName = null;

    #[Assert\All([
        new Assert\Positive(message: "Значения в поле filialIds должны быть положительными")
    ])]
    private ?array $filialIds = null;

    #[Assert\Email(message: "Поле email не является почтой")]
    private ?string $email = null;
    
    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getFilialIds(): ?array
    {
        return $this->filialIds;
    }

    public function setMiddleName(?string $middleName): UpdateProfileDto
    {
        $this->middleName = $middleName;
        return $this;
    }

    public function setLastName(?string $lastName): UpdateProfileDto
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function setFirstName(?string $firstName): UpdateProfileDto
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function setEmail(?string $email): UpdateProfileDto
    {
        $this->email = $email;
        return $this;
    }
    
    public function setFilialIds(?array $filialIds): UpdateProfileDto
    {
        $this->filialIds = $filialIds;
        return $this;
    }

}