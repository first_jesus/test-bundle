<?php

namespace Test\Bundle\SecurityBundle\Security;

use App\Common\Domain\Exception\NotFoundException;
use App\Common\Infrastructure\JsonResponseFormatter;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Test\Bundle\SecurityBundle\Entity\Profile;
use Test\Bundle\SecurityBundle\Entity\IProfileRepository;
use Test\Bundle\SecurityBundle\Exception\JWTFailureException;
use Test\Bundle\SecurityBundle\Exception\ProfileNotFoundException;
use Test\Bundle\SecurityBundle\Service\JWT\IJWTManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Psr\Log\LoggerInterface;
use GuzzleHttp\Exception\ClientException;

class PobedaTokenAuthenticator extends AbstractAuthenticator
{
    const TOKEN_HEADER_NAME = 'authorization';

    public function __construct(
        private readonly Client             $client,
        private readonly IJWTManager        $jwtService,
        private readonly IProfileRepository $repository,
        private readonly LoggerInterface    $logger
    )
    {
    }

    public function supports(Request $request): ?bool
    {
        return $request->headers->has(self::TOKEN_HEADER_NAME) && !empty($request->headers->get(self::TOKEN_HEADER_NAME));
    }

    /**
     * @throws NotFoundException
     * @throws JWTFailureException
     */
    public function authenticate(Request $request): Passport
    {
        if (!$_ENV['AUTH_URL']) {
            throw new NotFoundException('Необходимо добавить значение переменной AUTH_URL в .env');
        }

        $jwtToken = $this->getToken($request);

        try {
            $this->client->post(
                uri: $_ENV['AUTH_URL'] . '/auth',
                options: [
                    'body' => json_encode([
                        'accessToken' => $jwtToken
                    ])
                ]
            );

            $jwt = $this->jwtService->decode($jwtToken);
            $payload = $jwt->getPayload();

            if (!array_key_exists('userId', $payload)) {
                throw new CustomUserMessageAuthenticationException('Invalid token');
            }

            $roles = array_key_exists('roles', $payload) ? $this->prepareRoles($payload['roles']) : $this->prepareRoles([]);
            $userExtId = (int)$payload['userId'];

            return new SelfValidatingPassport(
                new UserBadge($userExtId, function () use ($userExtId, $jwtToken, $roles) {
                    /** @var Profile $profile */
                    $profile = $this->repository->getByUserExtId($userExtId);

                    if (!$profile) {
                        $this->logger->error("Security: Профиль c userExtId={$userExtId} не найден");
                        throw new ProfileNotFoundException('Security: Профиль не найден');
                    }

                    $profile->setAccessToken($jwtToken)->setRoles($roles);
                    return $profile;
                })
            );
        } catch (ClientException $e) {
            $response = json_decode($e->getResponse()->getBody()->getContents(), true);
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new JWTFailureException($response['message']);
        } catch (RequestException $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new CustomUserMessageAuthenticationException('Ошибка авторизации');
        } catch (GuzzleException $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new CustomUserMessageAuthenticationException('Не удалось авторизоваться');
        }
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return JsonResponseFormatter::unauthorized(strtr($exception->getMessageKey(), $exception->getMessageData()));
    }

    private function prepareRoles(array $roles = []): array
    {
        if (empty($roles)) {
            return ['ROLE_USER'];
        }

        return array_map(function (string $role) {
            return 'ROLE_' . $role;
        }, $roles);
    }

    private function getToken(Request $request): string
    {
        list(, $token) = explode(' ', $request->headers->get(self::TOKEN_HEADER_NAME));
        return $token;
    }
}