<?php

namespace Test\Bundle\SecurityBundle;

use Test\Bundle\SecurityBundle\DependencyInjection\PobedaSecurityExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class PobedaSecurityBundle extends Bundle
{

    public function getPath(): string
    {
        return dirname(__DIR__);
    }

    public function getContainerExtension(): ?ExtensionInterface
    {
        return new PobedaSecurityExtension();
    }

}