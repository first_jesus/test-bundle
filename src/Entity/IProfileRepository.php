<?php

namespace Test\Bundle\SecurityBundle\Entity;

interface IProfileRepository
{

    public function getByUserExtId(int $extId): ?Profile;

    public function getByUserPhone(string $phone): ?Profile;

    public function save(Profile $profile): int;

}