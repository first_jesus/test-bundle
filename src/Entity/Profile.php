<?php

namespace Test\Bundle\SecurityBundle\Entity;

use App\Common\Domain\TimestampsTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[ORM\Table(name: 'profile')]
#[ORM\Index(columns: ['user_id'])]
#[ORM\Index(columns: ['phone'])]
#[ORM\HasLifecycleCallbacks]
class Profile implements UserInterface
{

    use TimestampsTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[ORM\Column(name: 'id', type: 'integer')]
    private int $id;

    private array $roles = [];
    private string $accessToken = '';

    public function __construct(
        #[ORM\Column(name: 'phone', type: 'string', length: 50, unique: true)]
        #[Groups(['user', 'employee'])]
        private string       $phone,

        #[ORM\Column(name: 'user_id', type: 'integer', unique: true)]
        #[Groups(['user', 'employee'])]
        private readonly int $userId,

        #[ORM\Column(name: 'is_employee', type: 'boolean')]
        #[Groups(['user', 'employee'])]
        private bool         $isEmployee,

        #[ORM\Column(name: 'first_name', type: 'string', length: 50, nullable: true)]
        #[Groups(['user', 'employee'])]
        private ?string      $firstName = null,

        #[ORM\Column(name: 'middle_name', type: 'string', length: 50, nullable: true)]
        #[Groups(['user', 'employee'])]
        private ?string      $middleName = null,

        #[ORM\Column(name: 'last_name', type: 'string', length: 50, nullable: true)]
        #[Groups(['user', 'employee'])]
        private ?string      $lastName = null,

        #[ORM\Column(name: 'filial_ids', type: 'json', nullable: true, options: ['jsonb' => true])]
        #[Groups('employee')]
        private ?array       $filialIds = null,

        #[ORM\Column(name: 'email', type: 'string', length: 50, nullable: true)]
        #[Groups(['user', 'employee'])]
        private ?string      $email = null
    )
    {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getFilialIds(): ?array
    {
        return $this->filialIds;
    }

    public function isEmployee(): ?bool
    {
        return $this->isEmployee;
    }

    public function setFirstName(?string $firstName): Profile
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function setLastName(?string $lastName): Profile
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function setMiddleName(?string $middleName): Profile
    {
        $this->middleName = $middleName;
        return $this;
    }

    public function setPhone(string $phone): Profile
    {
        $this->phone = $phone;
        return $this;
    }

    public function setFilialIds(?array $filialIds): Profile
    {
        $this->filialIds = $filialIds;
        return $this;
    }

    public function setEmail(?string $email): Profile
    {
        $this->email = $email;
        return $this;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;
        return $this;
    }

    public function setAccessToken(string $accessToken): Profile
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    public function setIsEmployee(bool $isEmployee): Profile
    {
        $this->isEmployee = $isEmployee;
        return $this;
    }

    public function getIsUser(): bool
    {
        return in_array('ROLE_USER', $this->roles);
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getUserIdentifier(): string
    {
        return $this->userId;
    }

}