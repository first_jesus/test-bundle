<?php

namespace Test\Bundle\SecurityBundle\Controller;

use App\Common\Infrastructure\JsonResponseFormatter;
use App\Common\Infrastructure\Service\Serializer\ICommonSerializer;
use App\Common\Infrastructure\Service\Validator\Exception\ValidateException;
use Test\Bundle\SecurityBundle\Dto\SignUpEmployeeDto;
use Test\Bundle\SecurityBundle\Dto\UpdateProfileDto;
use Test\Bundle\SecurityBundle\Entity\IProfileRepository;
use Test\Bundle\SecurityBundle\Exception\AuthConnectorException;
use Test\Bundle\SecurityBundle\Factory\ProfileFactory;
use Test\Bundle\SecurityBundle\Service\AuthService\UserAuthService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/employee', name: 'profile_employee_')]
class EmployeeProfileController extends AbstractController
{

    public function __construct(
        private readonly ICommonSerializer  $serializer,
        private readonly IProfileRepository $repository,
        private readonly ProfileFactory     $factory,
        private readonly UserAuthService    $authService
    )
    {
    }

    /**
     * @throws AuthConnectorException
     * @throws ValidateException
     */
    #[Route(path: '', name: 'create', methods: ['POST'])]
    public function create(SignUpEmployeeDto $dto): JsonResponse
    {
        return JsonResponseFormatter::created([
            'userId' => $this->authService->signUp($dto)
        ]);
    }

    #[Route(path: '/{userId}', name: 'update', methods: ['PATCH'])]
    public function update(int $userId, UpdateProfileDto $dto): JsonResponse
    {
        $this->repository->save(
            $this->factory->update(
                profile: $this->repository->getByUserExtId($userId),
                dto: $dto
            )
        );
        return JsonResponseFormatter::ok();
    }

    #[Route(path: '', name: 'get', methods: ['GET'])]
    public function get(): JsonResponse
    {
        return JsonResponseFormatter::ok(json_decode(
            $this->serializer->objectToJson(
                $this->getUser(),
                ['groups' => 'employee']
            )
        ));
    }

}