<?php

namespace Test\Bundle\SecurityBundle\Repository\Provider;

use Test\Bundle\SecurityBundle\Entity\Profile;
use Test\Bundle\SecurityBundle\Entity\IProfileRepository;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class ProfileProvider implements UserProviderInterface
{

    public function __construct(
        private readonly IProfileRepository $repository,
    )
    {
    }

    public function refreshUser(UserInterface $user): UserInterface
    {
        return $user;
    }

    public function supportsClass(string $class): bool
    {
        return Profile::class === $class;
    }

    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        return $this->repository->getByUserExtId((int)$identifier);
    }
}