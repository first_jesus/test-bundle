<?php

namespace Test\Bundle\SecurityBundle\Repository\Db;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Test\Bundle\SecurityBundle\Entity\Profile;
use Test\Bundle\SecurityBundle\Entity\IProfileRepository;
use Test\Bundle\SecurityBundle\Exception\ProfileSaveException;
use Doctrine\ORM\EntityManagerInterface;
use Test\Bundle\SecurityBundle\Exception\ProfileNotFoundException;
use Psr\Log\LoggerInterface;

class ProfileRepository implements IProfileRepository
{

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly LoggerInterface        $logger
    )
    {
    }

    /**
     * @throws ProfileNotFoundException
     */
    public function getByUserExtId(int $extId): ?Profile
    {
        $profile = $this->em->getRepository(Profile::class)->findOneBy(['userId' => $extId]);

        if (!$profile) {
            $this->logger->error("Профиль с userId={$extId} не найден");
            throw new ProfileNotFoundException("Профиль с userId={$extId} не найден");
        }

        return $profile;
    }

    /**
     * @throws ProfileSaveException
     */
    public function save(Profile $profile): int
    {
        try {
            $this->em->persist($profile);
            $this->em->flush();
        } catch (ORMException|OptimisticLockException $e) {
            $this->logger->error('Не удалось сохранить профиль: ' . $e->getMessage());
            throw new ProfileSaveException('Не удалось сохранить профиль');
        } catch (UniqueConstraintViolationException $e) {
            $this->logger->error('Профиль уже существует: ' . $e->getMessage());
            throw new ProfileSaveException('Профиль уже существует');
        }

        return $profile->getId();
    }

    public function getByUserPhone(string $phone): ?Profile
    {
        return $this->em->getRepository(Profile::class)->findOneBy(['phone' => $phone]);
    }
}